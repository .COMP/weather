import 'package:weather/utils/constant.dart';

abstract class URL {
  static const BASE = "http://api.openweathermap.org/";
  static const WEATHER_URL =
      "${BASE}data/2.5/forecast?id=292223&appid=$API_KEY";
}
