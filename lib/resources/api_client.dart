import 'dart:convert';
import 'dart:developer';

import "package:dio/dio.dart";
import 'package:weather/controller/data_store.dart';
import 'package:weather/resources/url.dart';

class ValidationException implements Exception {
  final Map<String, dynamic> errors;

  ValidationException(this.errors);
}

class SimpleException implements Exception {
  final String _msg;

  SimpleException(this._msg);

  @override
  String toString() {
    return _msg;
  }
}

class BaseClient {
  Dio client = new Dio();

  BaseClient() {
    client.interceptors.add(ApiInterceptors());
    client.options.baseUrl = URL.BASE;
  }
}

class ApiInterceptors extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    log("------------ REQUEST -----------");
    log(options.path, name: "URL");
    log("${options.queryParameters}", name: "QUERY");
    log("${options.data}", name: "BODY");
    options.headers = {
      "Accept-Language": DataStore.instance.lang,
      "Content-Type": Headers.jsonContentType,
      "Authorization": DataStore.instance.token,
      "Accept": "application/json"
    };
    log("${options.headers}", name: "HEADERS");
    super.onRequest(options, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    log("------------ ERROR -----------");
    log("${err.message}", name: "ERROR");
    if (err.type == DioErrorType.response) {
      log("${jsonEncode(err.response.data)}", name: "ERROR");
      if (err.response.statusCode == 422) {
        var data = err.response.data;
        handler.reject(DioError(
          error: ValidationException(data["errors"]),
          requestOptions: err.requestOptions,
          response: err.response,
        ));
      } else if (err.response.statusCode == 400 &&
          err.response.statusCode == 403) {
        var data = err.response.data;
        handler.reject(DioError(
          error: SimpleException(data["message"]),
          requestOptions: err.requestOptions,
          response: err.response,
        ));
      } else {
        if (err.response.data == null) {
          handler.reject(err);
        } else {
          var data = err.response.data;
          handler.reject(DioError(
            error: SimpleException(data["message"]),
            requestOptions: err.requestOptions,
            response: err.response,
          ));
        }
      }
    } else if (err.type == DioErrorType.other) {
      handler.reject(DioError(
        error: SimpleException(err.error.message),
        requestOptions: err.requestOptions,
        response: err.response,
      ));
    } else {
      handler.reject(err);
    }
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    log("------------ RESPONSE -----------");
    log("${response.data}", name: "RESPONSE");
    if (response.data["cod"] == "200") {
      handler.resolve(response);
    } else {
      handler.reject(DioError(
        error: response.data["errors"],
        requestOptions: response.requestOptions,
        response: response,
      ));
    }
  }
}
