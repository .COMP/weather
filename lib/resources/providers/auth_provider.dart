// import '../api_client.dart';
// import '../url.dart';
//
// class AuthProvider extends BaseClient {
//   /// Defines a login.
//   /// Takes the values [phoneNumber] and [password] and returns user model
//   /// and token.
//   Future<LoginResponse> login(String phoneNumber, String password) async {
//     try {
//       final response = await client.post(
//         URL.LOGIN_URL,
//         data: FormData.fromMap({
//           'mobile_number': phoneNumber,
//           'password': password,
//         }),
//       );
//       return LoginResponse.fromJson(response.data);
//     } on DioError catch (ex) {
//       throw ex.error;
//     }
//   }
//
//   /// Defines a signup.
//   /// Takes the values [fullName] and [mobile] and [email] and [password]
//   /// and [national] and [governorate] inside [signupRequest] object
//   /// and returns user model, token
//   /// and session id.
//   Future<SignupResponseModel> signup(SignupRequest signupRequest) async {
//     try {
//       final response = await client.post(
//         URL.SIGNUP_URL,
//         data: FormData.fromMap(signupRequest.toJson()),
//       );
//       return SignupResponseModel.fromJson(response.data);
//     } on DioError catch (ex) {
//       if (ex.error is ValidationException) {
//         throw ex.error;
//       } else
//         throw ex.message;
//     }
//   }
//
//   /// Defines a verificationPin.
//   /// Takes the values [sessionId] and [pin] to verify account by [sessionId]
//   /// and returns user model.
//   Future<VerificationPinResponse> verificationPin(
//       String sessionId, String pin) async {
//     try {
//       final response = await client.post(URL.VERIFICATION_PIN_URL,
//           data: FormData.fromMap({"session_id": sessionId, "pin": pin}));
//       if (response.statusCode == 403) {
//         return VerificationPinResponse.fromJson(response.data);
//       }
//       return VerificationPinResponse.fromJson(response.data);
//     } on DioError catch (ex) {
//       VerificationPinResponse v = VerificationPinResponse(
//           code: ex.response.data['code'],
//           data: ex.response.data['data'],
//           message: ex.response.data['message']);
//       throw v.toJson();
//     }
//   }
//
//   /// Defines a re-send pin code.
//   /// Takes the values [token]
//   /// and returns empty response
//   Future<void> resendPinCode() async {
//     try {
//       return  await client.post(
//         URL.RESEND_PIN_CODE_URL,
//       );
//     } on DioError catch (ex) {
//       if (ex.error is ValidationException) {
//         throw ex.error;
//       } else
//         throw ex.message;
//     }
//   }
// }
