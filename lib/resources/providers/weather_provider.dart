import 'package:dio/dio.dart';
import 'package:weather/models/response/weather_response_model.dart';

import '../api_client.dart';
import '../url.dart';

class WeatherProvider extends BaseClient {
  /// Defines a get all weather.
  /// Doesn't takes any parameter
  /// and returns list of weather's model.
  Future<WeatherResponseModel> getWeather() async {
    try {
      final response = await client.post(
        URL.WEATHER_URL,
      );
      if (response.statusCode == 403) {
        return WeatherResponseModel.fromJson(response.data);
      }
      return WeatherResponseModel.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }
}
