import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:weather/bloc/global_state/global_state.dart';
import 'package:weather/bloc/weather_bloc/weather_bloc.dart';
import 'package:weather/localization/app_localization.dart';
import 'package:weather/styles/app_colors.dart';
import 'package:weather/styles/app_font_size.dart';
import 'package:weather/utils/utils.dart';
import 'package:weather/widget/common/empty_widget.dart';
import 'package:weather/widget/common/line_widget.dart';
import 'package:weather/widget/common/network_error_widget.dart';
import 'package:weather/widget/common/progress_widget.dart';

class WeatherUi extends StatefulWidget {
  final DateTime currentDateTime;

  const WeatherUi(this.currentDateTime);

  @override
  _WeatherUiState createState() => _WeatherUiState();
}

class _WeatherUiState extends State<WeatherUi> {
  WeatherBloc _weatherBloc;

  @override
  void initState() {
    super.initState();
    this._weatherBloc = BlocProvider.of(context);
    super.initState();
  }

  @override
  void dispose() {
    this._weatherBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return Stack(
      children: [
        Scaffold(
            body: BlocBuilder<WeatherBloc, GlobalState>(
          buildWhen: (prev, curr) {
            return curr is LoadingState ||
                curr is GotWeatherState ||
                curr is ErrorState;
          },
          builder: (context, state) {
            if (state is LoadingState) {
              return ProgressWidget(
                color: AppColors.lightBlue,
              );
            } else if (state is ErrorState) {
              return NetworkErrorWidget(
                message: state.message,
                onRefresh: () {
                  this
                      ._weatherBloc
                      .add(GettingWeatherEvent(widget.currentDateTime));
                },
              );
            }

            if (this._weatherBloc.weatherResponseModel == null) {
              return EmptyWidget(
                () {
                  this
                      ._weatherBloc
                      .add(GettingWeatherEvent(widget.currentDateTime));
                },
              );
            }
            return SingleChildScrollView(
              child: Container(
                height: mediaQuery.orientation == Orientation.portrait
                    ? mediaQuery.size.height
                    : null,
                color: AppColors.brown,
                child: Padding(
                  padding: Utils.weatherPagePadding(mediaQuery),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      // Title
                      Text(
                        AppLocalization.of(context).trans("sunny_title"),
                        style: Theme.of(context)
                            .textTheme
                            .headline2
                            .copyWith(color: AppColors.grayAccentLight),
                      ),

                      // Empty space
                      mediaQuery.orientation == Orientation.portrait
                          ? Spacer()
                          : SizedBox(
                              height: Utils.verticalSpace(mediaQuery),
                            ),

                      SvgPicture.asset(Utils.getSvg("sun.svg")),

                      // Empty space
                      mediaQuery.orientation == Orientation.portrait
                          ? Spacer()
                          : SizedBox(
                              height: Utils.verticalSpace(mediaQuery),
                            ),

                      // Footer
                      Text(
                        "Istanbul",
                        style: Theme.of(context).textTheme.headline2.copyWith(
                              fontWeight: FontWeight.normal,
                            ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text(
                              "35",
                              style: Theme.of(context)
                                  .textTheme
                                  .headline1
                                  .copyWith(
                                      fontWeight: FontWeight.normal,
                                      fontSize: AppFontSize.UX_LARGE * 2),
                            ),
                          ),
                          Row(
                            children: [
                              ColoredBox(
                                color: AppColors.white,
                                child: Padding(
                                  padding: Utils.paddingCardFooter(mediaQuery),
                                  child: Column(
                                    children: [
                                      Text(
                                        "Wind",
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1
                                            .copyWith(
                                                color: AppColors.lightGray),
                                      ),
                                      Text(
                                        "${this._weatherBloc.weatherResponseModel.wind.speed}km/h",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1
                                            .copyWith(
                                                color: AppColors.black,
                                                fontWeight: FontWeight.normal),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: Utils.horizontalSpace(mediaQuery),
                              ),
                              ColoredBox(
                                color: AppColors.white,
                                child: Padding(
                                  padding: Utils.paddingCardFooter(mediaQuery),
                                  child: Column(
                                    children: [
                                      Text(
                                        "Wind",
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1
                                            .copyWith(
                                                color: AppColors.lightGray),
                                      ),
                                      Text(
                                        "${this._weatherBloc.weatherResponseModel.main.humidity}%",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1
                                            .copyWith(
                                                color: AppColors.black,
                                                fontWeight: FontWeight.normal),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Utils.verticalSpace(mediaQuery),
                      ),

                      // Line
                      Align(
                        alignment: Alignment.center,
                        child: LineWidget(),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        )),
      ],
    );
  }
}
