import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather/bloc/choose_weather_bloc/choose_weather_bloc.dart';
import 'package:weather/bloc/global_state/global_state.dart';
import 'package:weather/bloc/weather_bloc/weather_bloc.dart';
import 'package:weather/localization/app_localization.dart';
import 'package:weather/styles/app_colors.dart';
import 'package:weather/ui/weather/weather_ui.dart';
import 'package:weather/utils/utils.dart';
import 'package:weather/widget/card/day_card_widget.dart';

class ChooseWeatherUi extends StatefulWidget {
  const ChooseWeatherUi({Key key}) : super(key: key);

  @override
  _ChooseWeatherUiState createState() => _ChooseWeatherUiState();
}

class _ChooseWeatherUiState extends State<ChooseWeatherUi> {
  ChooseWeatherBloc _chooseWeatherBloc;

  @override
  void initState() {
    this._chooseWeatherBloc = BlocProvider.of(context);
    this._chooseWeatherBloc.selectedValue = this._chooseWeatherBloc.days[0];
    super.initState();
  }

  @override
  void dispose() {
    this._chooseWeatherBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return Scaffold(
      backgroundColor: AppColors.white,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // Logo
              Image.asset(
                Utils.getImage("logo.png"),
                width: Utils.logoSizeInChoose(mediaQuery),
              ),
              SizedBox(
                height: Utils.verticalSpace(mediaQuery),
              ),

              // Day picker
              BlocBuilder<ChooseWeatherBloc, GlobalState>(
                buildWhen: (prev, curr) {
                  return curr is SelectDateState;
                },
                builder: (context, state) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: <Widget>[
                            DayCardWidget(
                              selectedValue:
                                  this._chooseWeatherBloc.selectedValue,
                              value: this._chooseWeatherBloc.days[0],
                              onTap: () {
                                this._chooseWeatherBloc.add(
                                      SelectDateEvent(
                                        this._chooseWeatherBloc.days[0],
                                      ),
                                    );
                              },
                            ),
                            DayCardWidget(
                              selectedValue:
                                  this._chooseWeatherBloc.selectedValue,
                              value: this._chooseWeatherBloc.days[1],
                              onTap: () {
                                this._chooseWeatherBloc.add(
                                      SelectDateEvent(
                                        this._chooseWeatherBloc.days[1],
                                      ),
                                    );
                              },
                            ),
                            DayCardWidget(
                              selectedValue:
                                  this._chooseWeatherBloc.selectedValue,
                              value: this._chooseWeatherBloc.days[2],
                              onTap: () {
                                this._chooseWeatherBloc.add(
                                      SelectDateEvent(
                                        this._chooseWeatherBloc.days[2],
                                      ),
                                    );
                              },
                            ),
                            DayCardWidget(
                              selectedValue:
                                  this._chooseWeatherBloc.selectedValue,
                              value: this._chooseWeatherBloc.days[3],
                              onTap: () {
                                this._chooseWeatherBloc.add(
                                      SelectDateEvent(
                                        this._chooseWeatherBloc.days[3],
                                      ),
                                    );
                              },
                            ),
                            DayCardWidget(
                              selectedValue:
                                  this._chooseWeatherBloc.selectedValue,
                              value: this._chooseWeatherBloc.days[4],
                              onTap: () {
                                this._chooseWeatherBloc.add(
                                      SelectDateEvent(
                                        this._chooseWeatherBloc.days[4],
                                      ),
                                    );
                              },
                            ),
                            DayCardWidget(
                              selectedValue:
                                  this._chooseWeatherBloc.selectedValue,
                              value: this._chooseWeatherBloc.days[5],
                              onTap: () {
                                this._chooseWeatherBloc.add(
                                      SelectDateEvent(
                                        this._chooseWeatherBloc.days[5],
                                      ),
                                    );
                              },
                            ),
                            // ...
                          ],
                        ),
                      ),
                    ],
                  );
                },
              ),
              SizedBox(
                height: Utils.verticalSpace(mediaQuery),
              ),

              // time picker
              Center(
                child: ElevatedButton(
                  onPressed: this._showDatePicker,
                  child: Text(
                    AppLocalization.of(context).trans("choose_date"),
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: AppColors.darkBlue,
                  ),
                ),
              ),
              SizedBox(
                height: Utils.verticalSpace(mediaQuery),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _showDatePicker() async {
    TimeOfDay selectedTimeRTL = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
      builder: (BuildContext context, Widget child) {
        return Directionality(
          textDirection: TextDirection.rtl,
          child: child,
        );
      },
    );
    this._chooseWeatherBloc.selectedTime = selectedTimeRTL;
    this._chooseWeatherBloc.selectedValue = DateTime(
      this._chooseWeatherBloc.selectedValue.year,
      this._chooseWeatherBloc.selectedValue.month,
      this._chooseWeatherBloc.selectedValue.day,
      this._chooseWeatherBloc.selectedTime.hour,
      this._chooseWeatherBloc.selectedTime.minute,
    );
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => BlocProvider<WeatherBloc>(
          create: (context) => WeatherBloc()
            ..add(GettingWeatherEvent(this._chooseWeatherBloc.selectedValue)),
          child: WeatherUi(this._chooseWeatherBloc.selectedValue),
        ),
      ),
    );

    // DatePicker.showDatePicker(
    //   context,
    //   showTitleActions: true,
    //   minTime: DateTime.now(),
    //   maxTime: DateTime.now().add(const Duration(days: 5)),
    //   theme: DatePickerTheme(
    //     headerColor: AppColors.grayAccentLight,
    //     backgroundColor: AppColors.white,
    //     itemStyle: Theme.of(context)
    //         .textTheme
    //         .bodyText1
    //         .copyWith(color: AppColors.black),
    //     doneStyle: Theme.of(context)
    //         .textTheme
    //         .bodyText1
    //         .copyWith(color: AppColors.black),
    //   ),
    //   onChanged: (date) {
    //     print('change $date in time zone ' +
    //         date.timeZoneOffset.inHours.toString());
    //   },
    //   onConfirm: (date) {
    //     print('confirm $date');
    //     this._dateTime = Utils.formatDateTime(date);
    //     Navigator.push(
    //       context,
    //       MaterialPageRoute(
    //         builder: (context) => BlocProvider<WeatherBloc>(
    //           create: (context) =>
    //               WeatherBloc()..add(GettingWeatherEvent(this._dateTime)),
    //           child: WeatherUi(),
    //         ),
    //       ),
    //     );
    //   },
    //   currentTime: DateTime.now(),
    //   locale: LocaleType.en,
    // );
  }
}
