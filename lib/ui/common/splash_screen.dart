import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:weather/bloc/splash_view_model/splash_view_model.dart';
import 'package:weather/localization/app_localization.dart';
import 'package:weather/styles/app_colors.dart';
import 'package:weather/utils/enums/enums.dart';
import 'package:weather/utils/utils.dart';

class SplashScreen extends StatefulWidget {
  final String iconImage;
  final Duration duration;

  const SplashScreen({
    Key key,
    this.duration = const Duration(seconds: 1, milliseconds: 600),
    this.iconImage = "assets/images/logo.png",
  }) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  AnimationController sizeFadeAnimationController;
  Animation<double> _scaleAnimation;
  Animation<double> _fadeAnimation;

  // Splash view model without event, state and bloc because there is no widget
  // need to update state in splash screen
  final viewModel = SplashViewModel();

  @override
  void initState() {
    _initAnimation();
    _initUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Scaffold(
      backgroundColor: AppColors.white,
      body: Center(
        child: SizedBox(
          width: mediaQuery.size.width * 0.8,
          child: AnimatedBuilder(
            animation: sizeFadeAnimationController,
            builder: (context, _) {
              return FadeTransition(
                opacity: _fadeAnimation,
                child: Transform.scale(
                  scale: _scaleAnimation.value,
                  child: Image.asset(
                    widget.iconImage,
                    alignment: Alignment.center,
                    width: Utils.backgroundLogoSize(mediaQuery),
                    height: Utils.backgroundLogoSize(mediaQuery),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  @override
  dispose() {
    sizeFadeAnimationController?.dispose();
    super.dispose();
  }

  _initUser() async {
    UserStatus _status;
    var timer = Timer(widget.duration, () {
      if (_status != null) _navigate(_status);
    });
    try {
      _status = await viewModel.checkingUserStatus();
      if (!timer.isActive) _navigate(_status);
    } catch (e) {
      showErrorDialog();
    }
  }

  void showErrorDialog() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (ctx) {
        return Dialog(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  AppLocalization.of(context).trans("sorry_error"),
                  style: Theme.of(context).textTheme.headline6,
                ),
                SizedBox(height: 15),
                Align(
                  alignment: AlignmentDirectional.centerEnd,
                  child: InkWell(
                    onTap: () {
                      exit(0);
                    },
                    child: Text(
                      AppLocalization.of(context).trans("ok"),
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _navigate(UserStatus state) {
    if (state == UserStatus.active) {
      // TODO Navigate to specific page
    } else {
      Navigator.pushNamedAndRemoveUntil(
        context,
        "/home",
        (route) => false,
      );
    }
  }

  _initAnimation() {
    sizeFadeAnimationController = AnimationController(
      duration: widget.duration,
      vsync: this,
    );
    _scaleAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: sizeFadeAnimationController,
        curve: Interval(0.0, 1.0, curve: Curves.bounceOut),
      ),
    );
    _fadeAnimation = Tween<double>(begin: 5.0, end: 1.0).animate(
      sizeFadeAnimationController,
    );
    sizeFadeAnimationController.forward();
  }
}
