import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class DataStore {
  FlutterSecureStorage _storage = FlutterSecureStorage();

  static final DataStore _instance = DataStore._internal();

  static DataStore get instance => _instance;

  DataStore._internal();

  String _token;

  //TODO remove sessionId function's
  String sessionId;
  String _lang;

  String get token => _token == null ? null : "Bearer $_token";

  String get lang => _lang;

  Future<void> init() async {
    _token = await _storage.read(key: 'token');

    sessionId = await _storage.read(key: 'session_id');
    sessionId = sessionId == null ? null : sessionId;

    _lang = await _storage.read(key: 'lang');
    _lang = lang == null ? "en" : lang;

    print("Data store initialized");
  }

  bool get hasToken => token != null;

  // token
  Future<void> deleteToken() async {
    _token = null;
    return await _storage.delete(key: "token");
  }

  Future<void> putToken(String value) async {
    _token = value;
    await _storage.write(key: "token", value: value);
  }

  // Lang
  Future<void> deleteLang() async {
    _lang = null;
    await _storage.delete(key: "lang");
  }

  Future<void> putLang(String value) async {
    _lang = value;
    await _storage.write(key: "lang", value: value);
  }

  // session id
  Future<void> deleteSessionId() async =>
      await _storage.delete(key: "session_id");

  Future<void> putSessionId(String value) async {
    this.sessionId = value;
    return await _storage.write(key: "session_id", value: value);
  }

  Future<void> putValue(String key, String value) async {
    await _storage.write(key: key, value: value);
  }

  Future<String> getValue(String key) async {
    return await _storage.read(
      key: key,
    );
  }

  Future<void> deleteValue(String key) async {
    return await _storage.delete(key: key);
  }
}
