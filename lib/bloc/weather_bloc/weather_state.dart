part of 'weather_bloc.dart';

@immutable
abstract class WeatherState extends GlobalState {}

// state for get all weather's
class GotWeatherState extends WeatherState {}
