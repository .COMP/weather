part of 'weather_bloc.dart';

@immutable
abstract class WeatherEvent {}

// event for get all weather's
class GettingWeatherEvent extends WeatherEvent {
  final DateTime dateTime;

  GettingWeatherEvent(this.dateTime);
}
