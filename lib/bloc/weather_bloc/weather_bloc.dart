import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';
import 'package:weather/bloc/global_state/global_state.dart';
import 'package:weather/controller/data_store.dart';
import 'package:weather/models/common/base_locale_model.dart';
import 'package:weather/models/common/list_item_model.dart';
import 'package:weather/resources/providers/weather_provider.dart';
import 'package:weather/utils/constant.dart';
import 'package:weather/utils/utils.dart';

part 'weather_event.dart';
part 'weather_state.dart';

class WeatherBloc extends Bloc<WeatherEvent, GlobalState> {
  WeatherBloc() : super(LoadingState());
  ListElementModel weatherResponseModel;

  @override
  Stream<GlobalState> mapEventToState(
    WeatherEvent event,
  ) async* {
    if (event is GettingWeatherEvent) {
      yield LoadingState();
      try {
        // Get from cache
        String jsonWeather =
            await DataStore.instance.getValue(event.dateTime.toString());
        // if data with specific args is not cached
        if (jsonWeather == null) {
          var response = await GetIt.I.get<WeatherProvider>().getWeather();
          // filter data to get item with specific date
          response.list.removeWhere((element) =>
              Utils.formatDateTime(element.dtTxt).toString() !=
              Utils.formatDateTime(event.dateTime).toString());

          // If data is exist, then cache it
          if (response.list.isNotEmpty) {
            this.weatherResponseModel = response.list[0];

            // Encapsulate data in base local model to cached it
            BaseLocalModel baseLocalModel = BaseLocalModel(
                time: Utils.formatDateTime(
                  Utils.formatDateTime(DateTime.now()).add(
                    Duration(
                      minutes: CACHED_TIME,
                    ),
                  ),
                ),
                model: json.encode(this.weatherResponseModel.toJson()));

            // Store data
            DataStore.instance.putValue(
              event.dateTime.toString(),
              json.encode(
                baseLocalModel.toJson(),
              ),
            );
          } else {
            this.weatherResponseModel = null;
          }
        } else {
          // if data with specific args is already cached
          String baseModelJson =
              await DataStore.instance.getValue(event.dateTime.toString());
          BaseLocalModel baseLocalModel = BaseLocalModel.fromJson(
            json.decode(baseModelJson),
          );

          this.weatherResponseModel = ListElementModel.fromJson(
            json.decode(baseLocalModel.model),
          );

          // check if cache need update
          if (Utils.formatDateTime(DateTime.now())
              .isAfter(baseLocalModel.time)) {
            DataStore.instance.deleteValue(
              event.dateTime.toString(),
            );
          }
        }
        yield GotWeatherState();
      } catch (ex) {
        yield ErrorState(ex.toString());
      }
    }
  }
}
