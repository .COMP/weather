import 'package:dio/dio.dart';
import 'package:weather/controller/data_store.dart';
import 'package:weather/utils/enums/enums.dart';

class SplashViewModel {
  Future<UserStatus> checkingUserStatus() async {
    if (DataStore.instance.hasToken) {
      try {
        /// TODO Check user status before navigate to home page
        // var user = await GetIt.I.get<UserProvider>().getUserProfile();
        // GetIt.I.registerSingleton<UserModel>(user);
        return UserStatus.active;
      } on DioError catch (ex) {
        if (ex.response.statusCode == 406) {
          return UserStatus.active;
        } else {
          throw ex.message;
        }
      }
    } else {
      return UserStatus.unauthenticated;
    }
  }
}
