// part 'login_event.dart';
//
// part 'login_state.dart';
//
// class LoginBloc extends Bloc<LoginEvent, GlobalState> {
//   final phoneNumberRegex = RegExp(PHONE_REGEX, caseSensitive: false);
//
//   final mobileNumberFocusNode = FocusNode();
//   final passwordFocusNode = FocusNode();
//
//   final mobileNumberController = TextEditingController();
//   final passwordController = TextEditingController();
//
//   final BuildContext context;
//
//   LoginBloc(this.context) : super(InitialState()) {
//     mobileNumberFocusNode.addListener(() {
//       if (!mobileNumberFocusNode.hasFocus) add(LoginFormValidationEvent());
//     });
//
//     passwordFocusNode.addListener(() {
//       if (!passwordFocusNode.hasFocus) add(LoginFormValidationEvent());
//     });
//   }
//
//   Future<void> close() async {
//     mobileNumberFocusNode.dispose();
//     passwordFocusNode.dispose();
//     super.close();
//   }
//
//   // if password text field is obscure text or not
//   bool isPassword = true;
//
//   // Text field controller
//
//   // Form validation result
//   bool isValidMobileNumber = true;
//   bool isValidPassword = true;
//
//   //
//   // // Form error message
//   String errorMobileNumberMsg;
//   String errorPasswordMsg;
//
//   bool _validate() {
//     // Form validation value
//     String mobileNumber = mobileNumberController.text.trim();
//     String password = passwordController.text.trim();
//
//     // Form validation result
//     isValidMobileNumber = phoneNumberRegex.hasMatch(mobileNumber);
//     isValidPassword = password.isNotEmpty;
//
//     errorMobileNumberMsg = mobileNumber.isEmpty
//         ? AppLocalization.of(context).trans("filed_required_msg")
//         : !isValidMobileNumber
//             ? AppLocalization.of(context).trans("error_mobile_number_msg")
//             : null;
//
//     errorPasswordMsg = password.isEmpty
//         ? AppLocalization.of(context).trans("filed_required_msg")
//         : null;
//
//     return isValidMobileNumber && isValidPassword;
//   }
//
//   @override
//   Stream<GlobalState> mapEventToState(LoginEvent event) async* {
//     if (event is LoginFormValidationEvent) {
//       _validate();
//       yield LoginFormValidationState();
//     } else if (event is OnLoggingEvent) {
//       if (!_validate()) {
//         yield LoginFormValidationState();
//       } else {
//         yield LoadingState();
//         try {
//           var response = await GetIt.I
//               .get<AuthProvider>()
//               .login(mobileNumberController.text, passwordController.text);
//           DataStore.instance.putToken(response.data.token);
//           GetIt.I.registerSingleton<UserModel>(response.data.user);
//           yield LoggedInState();
//         } on ValidationException catch (e) {
//           var errors = e.errors;
//           if (errors['mobile_number'] != null) {
//             isValidMobileNumber = false;
//             errorMobileNumberMsg = errors['mobile_number'].join(" ");
//           }
//           if (errors['password'] != null) {
//             isValidPassword = false;
//             errorPasswordMsg = errors['password'].join(" ");
//           }
//           yield LoginFormValidationState();
//         } catch (e) {
//           yield InitialState();
//           Fluttertoast.showToast(msg: e.toString());
//         }
//       }
//     } else if (event is LoginShowPasswordEvent) {
//       isPassword = !isPassword;
//       yield LoginShowPasswordState();
//     }
//   }
// }
