import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:weather/bloc/global_state/global_state.dart';
import 'package:weather/utils/utils.dart';

part 'choose_weather_event.dart';
part 'choose_weather_state.dart';

class ChooseWeatherBloc extends Bloc<ChooseWeatherEvent, GlobalState> {
  ChooseWeatherBloc() : super(InitialState());
  DateTime selectedValue = Utils.formatDateTime(DateTime.now());
  TimeOfDay selectedTime = TimeOfDay.now();
  List<DateTime> days = [
    Utils.formatDateTime(DateTime.now()),
    Utils.formatDateTime(DateTime.now().add(const Duration(days: 1))),
    Utils.formatDateTime(DateTime.now().add(const Duration(days: 2))),
    Utils.formatDateTime(DateTime.now().add(const Duration(days: 3))),
    Utils.formatDateTime(DateTime.now().add(const Duration(days: 4))),
    Utils.formatDateTime(DateTime.now().add(const Duration(days: 5))),
  ];

  @override
  Stream<GlobalState> mapEventToState(
    ChooseWeatherEvent event,
  ) async* {
    if (event is SelectDateEvent) {
      this.selectedValue = event.selectValue;
      yield SelectDateState();
    }
  }
}
