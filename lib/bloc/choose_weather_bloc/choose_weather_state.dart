part of 'choose_weather_bloc.dart';

@immutable
abstract class ChooseWeatherState extends GlobalState {}

// state for select date
class SelectDateState extends ChooseWeatherState {}
