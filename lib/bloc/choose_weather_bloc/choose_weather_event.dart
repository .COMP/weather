part of 'choose_weather_bloc.dart';

@immutable
abstract class ChooseWeatherEvent {}

// event for select date
class SelectDateEvent extends ChooseWeatherEvent {
  final DateTime selectValue;

  SelectDateEvent(this.selectValue);
}
