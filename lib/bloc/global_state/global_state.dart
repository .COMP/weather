abstract class GlobalState {}

class InitialState extends GlobalState {}

class LoadingState extends GlobalState {}

class ErrorState extends GlobalState {
  final String message;

  ErrorState(this.message);
}

class ValidationErrorState extends GlobalState {
  final Map<String, dynamic> error;

  ValidationErrorState(this.error);
}
