import 'dart:ui';

import 'package:flutter/material.dart';

class AppColors {
  static const white = Color(0xFFFFFFFF);
  static const grayAccentLight = Color(0xFFF2F2F2);
  static const gray = Color(0xFFAFAFAF);
  static const deepGray = Color(0xFF707070);
  static const black = Color(0xFF000000);
  static const redAccent = Color(0xFFD46265);
  static const red = Color(0xFFD93B58);
  static const green = Color(0xFF4CAF50);
  static const darkGreen = Color(0xFF17A655);
  static const blueAccent = Color(0xFF55bbff);
  static const blue = Color(0xFF00B4CE);
  static const grayAccent = Color(0xFFdae0ee);
  static const yellowAccent = Color(0xFFffee80);
  static const orangeAccent = Color(0xFFff9852);
  static const deepPurple = Color(0xFF701685);
  static const deepBlue = Color(0xFF1a0066);
  static const purple = Color(0xFF774ebe);
  static const lightPurple = Color(0xFF754cbc);
  static const lightOrange = Color(0xFFffa456);
  static final lightGray = Color(0xFF999999);
  static final transparent = Color(0x00000000);
  static final brown = Color(0xFFEEBC99);
  static final darkBlue = Color(0xFF0D4475);
  static final lightBlue = Color(0xFF62B7FF);
}
