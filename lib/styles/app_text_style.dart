import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'app_colors.dart';
import 'app_font_size.dart';

class AppTextStyle {
  static final xxLargeBlack = TextStyle(
    fontSize: AppFontSize.XX_LARGE,
    color: AppColors.black,
    fontWeight: FontWeight.normal,
  );

  static final xLargeWhite = TextStyle(
    fontSize: AppFontSize.X_LARGE,
    color: AppColors.white,
    fontWeight: FontWeight.normal,
  );

  static final xxLargeWhite = TextStyle(
    fontSize: AppFontSize.XX_LARGE,
    color: AppColors.white,
  );

  static final xxLargeWhiteBold = TextStyle(
    fontSize: AppFontSize.XX_LARGE,
    color: AppColors.white,
    fontWeight: FontWeight.bold,
  );

  static final largeWhiteBold = TextStyle(
    fontSize: AppFontSize.LARGE,
    color: AppColors.white,
    fontWeight: FontWeight.bold,
  );

  static final largeWhite = TextStyle(
    fontSize: AppFontSize.LARGE,
    color: AppColors.white,
  );

  static final mediumWhite = TextStyle(
    fontSize: AppFontSize.MEDIUM,
    color: AppColors.white,
  );

  static final mediumWhiteBold = TextStyle(
    fontSize: AppFontSize.MEDIUM,
    color: AppColors.white,
    fontWeight: FontWeight.bold,
  );

  static final smallWhite = TextStyle(
    fontSize: AppFontSize.SMALL,
    color: AppColors.white,
  );

  static final smallWhiteBold = TextStyle(
    fontSize: AppFontSize.SMALL,
    color: AppColors.white,
    fontWeight: FontWeight.bold,
  );

  static final xSmallWhite = TextStyle(
    fontSize: AppFontSize.X_SMALL,
    color: Colors.white,
  );

  static final xSmallWhiteBold = TextStyle(
    fontSize: AppFontSize.X_SMALL,
    color: Colors.white,
    fontWeight: FontWeight.bold,
  );

  static final xxSmallWhite = TextStyle(
    fontSize: AppFontSize.XX_SMALL,
    color: AppColors.white,
  );

//------------------------ gray
  static final mediumGrayBold = TextStyle(
    color: AppColors.gray,
    fontSize: AppFontSize.MEDIUM,
    fontWeight: FontWeight.bold,
  );

  static final smallGray = TextStyle(
    color: AppColors.gray,
    fontSize: AppFontSize.SMALL,
  );

  static final mediumGray = TextStyle(
    color: AppColors.gray,
    fontSize: AppFontSize.MEDIUM,
  );

  static final largeGrayBold = TextStyle(
    color: AppColors.gray,
    fontSize: AppFontSize.LARGE,
    fontWeight: FontWeight.bold,
  );

  static final mediumBlack = TextStyle(
      fontSize: AppFontSize.MEDIUM,
      color: AppColors.black,
      fontWeight: FontWeight.normal);

  static final mediumBlackBold = TextStyle(
    fontSize: AppFontSize.MEDIUM,
    color: AppColors.black,
    fontWeight: FontWeight.bold,
  );

  static final smallBlack = TextStyle(
    fontSize: AppFontSize.SMALL,
    color: AppColors.black,
  );

  static final smallBlackBold = TextStyle(
    fontSize: AppFontSize.SMALL,
    color: AppColors.black,
    fontWeight: FontWeight.bold,
  );

  static final largeBlackBold = TextStyle(
    fontSize: AppFontSize.LARGE,
    color: AppColors.black,
    fontWeight: FontWeight.bold,
  );

  static final xLargeBlackBold = TextStyle(
    fontSize: AppFontSize.X_LARGE,
    color: AppColors.black,
    fontWeight: FontWeight.bold,
  );

  static final xxLargeRed = TextStyle(
    fontSize: AppFontSize.XX_LARGE,
    color: AppColors.red,
  );

  static final xLargeGray = TextStyle(
    fontSize: AppFontSize.X_LARGE,
    color: AppColors.gray,
  );

  static final mediumRed = TextStyle(
    fontSize: AppFontSize.MEDIUM,
    color: AppColors.red,
  );

  static final xLargeRed = TextStyle(
    fontSize: AppFontSize.X_LARGE,
    color: AppColors.red,
    fontWeight: FontWeight.normal,
  );

  static final xxLargeBlue = TextStyle(
    fontSize: AppFontSize.XX_LARGE,
    color: AppColors.blue,
  );

  static final xxLargeBlueBold = TextStyle(
    fontSize: AppFontSize.XX_LARGE,
    color: AppColors.blue,
    fontWeight: FontWeight.bold,
  );

  static final xLargeBlue = TextStyle(
    fontSize: AppFontSize.X_LARGE,
    color: AppColors.blue,
  );

  static final xLargeBlack = TextStyle(
    fontSize: AppFontSize.X_LARGE,
    color: AppColors.black,
    fontWeight: FontWeight.normal,
  );

  static final largeBlack = TextStyle(
    fontSize: AppFontSize.LARGE,
    color: AppColors.black,
    fontWeight: FontWeight.normal,
  );

  static final xSmallBlackBold = TextStyle(
    fontSize: AppFontSize.X_SMALL,
    color: AppColors.black,
    fontWeight: FontWeight.bold,
  );

  static final xSmallBlack = TextStyle(
    fontSize: AppFontSize.X_SMALL,
    color: AppColors.black,
  );

  static final xxSmallBlack = TextStyle(
    fontSize: AppFontSize.XX_SMALL,
    color: AppColors.black,
  );

////////////////////// blue

  static final smallBlue = TextStyle(
    color: AppColors.blue,
    fontSize: AppFontSize.SMALL,
  );

  static final smallBlueBold = TextStyle(
    color: AppColors.blue,
    fontSize: AppFontSize.SMALL,
    fontWeight: FontWeight.bold,
  );

  static final mediumBlueBold = TextStyle(
    color: AppColors.blue,
    fontSize: AppFontSize.MEDIUM,
    fontWeight: FontWeight.bold,
  );

  static final mediumBlue = TextStyle(
    color: AppColors.blue,
    fontSize: AppFontSize.MEDIUM,
  );

  static final xlargeBlue = TextStyle(
    color: AppColors.blue,
    fontSize: AppFontSize.X_LARGE,
  );

  static final xlargeBlueBold = TextStyle(
    color: AppColors.blue,
    fontSize: AppFontSize.X_LARGE,
    fontWeight: FontWeight.bold,
  );

  static final largeBlue = TextStyle(
    color: AppColors.blue,
    fontSize: AppFontSize.LARGE,
  );

  static final xxxLargeBlack = TextStyle(
    fontSize: AppFontSize.XXX_LARGE,
    color: AppColors.black,
    fontWeight: FontWeight.normal,
  );

  static final xxxLargeBlackBold = TextStyle(
    fontSize: AppFontSize.XXX_LARGE,
    color: AppColors.black,
    fontWeight: FontWeight.bold,
  );

  static final xxxLargeWhite = TextStyle(
    fontSize: AppFontSize.XXX_LARGE,
    color: AppColors.white,
    fontWeight: FontWeight.normal,
  );

  static final xxxLargeWhiteBold = TextStyle(
    fontSize: AppFontSize.UX_LARGE,
    color: AppColors.white,
    fontWeight: FontWeight.bold,
  );
}
