import 'package:flutter/material.dart';

import 'app_colors.dart';
import 'app_text_style.dart';

final ThemeData appTheme = ThemeData(
  accentColor: AppColors.blue,
  appBarTheme: AppBarTheme(
    color: AppColors.white,
    elevation: 0,
    textTheme: TextTheme(
      headline6: AppTextStyle.xxxLargeBlack,
    ),
  ),
  iconTheme: IconThemeData(color: AppColors.black, size: 25),
  textTheme: TextTheme(
    headline1: AppTextStyle.xxxLargeWhiteBold,
    headline2: AppTextStyle.xxxLargeWhite,
    headline3: AppTextStyle.xxLargeBlack,
    headline4: AppTextStyle.xxLargeBlueBold,
    headline5: AppTextStyle.xLargeBlue,
    headline6: AppTextStyle.xLargeWhite,
    subtitle1: AppTextStyle.smallWhite,
    bodyText1: AppTextStyle.largeWhite,
    bodyText2: AppTextStyle.mediumWhite,
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      primary: AppColors.blue,
      padding: EdgeInsets.symmetric(
        horizontal: 25,
        vertical: 12,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
    ),
  ),
);
