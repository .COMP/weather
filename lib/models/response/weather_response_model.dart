import 'dart:convert';

import 'package:weather/models/common/city_model.dart';
import 'package:weather/models/common/list_item_model.dart';

class WeatherResponseModel {
  WeatherResponseModel({
    this.cod,
    this.message,
    this.cnt,
    this.list,
    this.city,
  });

  String cod;
  int message;
  int cnt;
  List<ListElementModel> list;
  CityModel city;

  factory WeatherResponseModel.fromRawJson(String str) =>
      WeatherResponseModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory WeatherResponseModel.fromJson(Map<String, dynamic> json) =>
      WeatherResponseModel(
        cod: json["cod"] == null ? null : json["cod"],
        message: json["message"] == null ? null : json["message"],
        cnt: json["cnt"] == null ? null : json["cnt"],
        list: json["list"] == null
            ? null
            : List<ListElementModel>.from(
                json["list"].map((x) => ListElementModel.fromJson(x))),
        city: json["city"] == null ? null : CityModel.fromJson(json["city"]),
      );

  Map<String, dynamic> toJson() => {
        "cod": cod == null ? null : cod,
        "message": message == null ? null : message,
        "cnt": cnt == null ? null : cnt,
        "list": list == null
            ? null
            : List<dynamic>.from(list.map((x) => x.toJson())),
        "city": city == null ? null : city.toJson(),
      };
}
