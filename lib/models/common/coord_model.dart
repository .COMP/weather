import 'dart:convert';

class CoordModel {
  CoordModel({
    this.lat,
    this.lon,
  });

  double lat;
  double lon;

  factory CoordModel.fromRawJson(String str) =>
      CoordModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CoordModel.fromJson(Map<String, dynamic> json) => CoordModel(
        lat: json["lat"] == null ? null : json["lat"].toDouble(),
        lon: json["lon"] == null ? null : json["lon"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "lat": lat == null ? null : lat,
        "lon": lon == null ? null : lon,
      };
}
