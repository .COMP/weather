import 'dart:convert';

class RainModel {
  RainModel({
    this.the3H,
  });

  double the3H;

  factory RainModel.fromRawJson(String str) =>
      RainModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory RainModel.fromJson(Map<String, dynamic> json) => RainModel(
        the3H: json["3h"] == null ? null : json["3h"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "3h": the3H == null ? null : the3H,
      };
}
