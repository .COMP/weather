import 'dart:convert';

import 'package:weather/utils/enums/constants.dart';
import 'package:weather/utils/enums/enums.dart';

class SysModel {
  SysModel({
    this.pod,
  });

  Pod pod;

  factory SysModel.fromRawJson(String str) =>
      SysModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory SysModel.fromJson(Map<String, dynamic> json) => SysModel(
        pod: json["pod"] == null ? null : podValues.map[json["pod"]],
      );

  Map<String, dynamic> toJson() => {
        "pod": pod == null ? null : podValues.reverse[pod],
      };
}
