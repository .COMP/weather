import 'dart:convert';

import 'package:weather/models/common/rain_model.dart';
import 'package:weather/models/common/sys_model.dart';
import 'package:weather/models/common/weather_model.dart';
import 'package:weather/models/common/wind_model.dart';
import 'package:weather/utils/utils.dart';

import 'clouds_model.dart';
import 'main_class_model.dart';

class ListElementModel {
  ListElementModel({
    this.dt,
    this.main,
    this.weather,
    this.clouds,
    this.wind,
    this.visibility,
    this.pop,
    this.sys,
    this.dtTxt,
    this.rain,
  });

  int dt;
  MainClassModel main;
  List<WeatherModel> weather;
  CloudsModel clouds;
  WindModel wind;
  int visibility;
  double pop;
  SysModel sys;
  DateTime dtTxt;
  RainModel rain;

  factory ListElementModel.fromRawJson(String str) =>
      ListElementModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ListElementModel.fromJson(Map<String, dynamic> json) =>
      ListElementModel(
        dt: json["dt"] == null ? null : json["dt"],
        main:
            json["main"] == null ? null : MainClassModel.fromJson(json["main"]),
        weather: json["weather"] == null
            ? null
            : List<WeatherModel>.from(
                json["weather"].map((x) => WeatherModel.fromJson(x))),
        clouds: json["clouds"] == null
            ? null
            : CloudsModel.fromJson(json["clouds"]),
        wind: json["wind"] == null ? null : WindModel.fromJson(json["wind"]),
        visibility: json["visibility"] == null ? null : json["visibility"],
        pop: json["pop"] == null ? null : json["pop"].toDouble(),
        sys: json["sys"] == null ? null : SysModel.fromJson(json["sys"]),
        dtTxt: json["dt_txt"] == null
            ? null
            : Utils.formatDateTime(DateTime.parse(json["dt_txt"])),
        rain: json["rain"] == null ? null : RainModel.fromJson(json["rain"]),
      );

  Map<String, dynamic> toJson() => {
        "dt": dt == null ? null : dt,
        "main": main == null ? null : main.toJson(),
        "weather": weather == null
            ? null
            : List<dynamic>.from(weather.map((x) => x.toJson())),
        "clouds": clouds == null ? null : clouds.toJson(),
        "wind": wind == null ? null : wind.toJson(),
        "visibility": visibility == null ? null : visibility,
        "pop": pop == null ? null : pop,
        "sys": sys == null ? null : sys.toJson(),
        "dt_txt": dtTxt == null ? null : dtTxt.toString(),
        "rain": rain == null ? null : rain.toJson(),
      };
}
