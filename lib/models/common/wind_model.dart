import 'dart:convert';

class WindModel {
  WindModel({
    this.speed,
    this.deg,
    this.gust,
  });

  double speed;
  int deg;
  double gust;

  factory WindModel.fromRawJson(String str) =>
      WindModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory WindModel.fromJson(Map<String, dynamic> json) => WindModel(
        speed: json["speed"] == null ? null : json["speed"].toDouble(),
        deg: json["deg"] == null ? null : json["deg"],
        gust: json["gust"] == null ? null : json["gust"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "speed": speed == null ? null : speed,
        "deg": deg == null ? null : deg,
        "gust": gust == null ? null : gust,
      };
}
