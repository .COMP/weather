import 'dart:convert';

import 'package:weather/utils/enums/constants.dart';
import 'package:weather/utils/enums/enums.dart';

class WeatherModel {
  WeatherModel({
    this.id,
    this.main,
    this.description,
    this.icon,
  });

  int id;
  MainEnum main;
  Description description;
  String icon;

  factory WeatherModel.fromRawJson(String str) =>
      WeatherModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory WeatherModel.fromJson(Map<String, dynamic> json) => WeatherModel(
        id: json["id"] == null ? null : json["id"],
        main: json["main"] == null ? null : mainEnumValues.map[json["main"]],
        description: json["description"] == null
            ? null
            : descriptionValues.map[json["description"]],
        icon: json["icon"] == null ? null : json["icon"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "main": main == null ? null : mainEnumValues.reverse[main],
        "description":
            description == null ? null : descriptionValues.reverse[description],
        "icon": icon == null ? null : icon,
      };
}
