import 'dart:convert';

class CloudsModel {
  CloudsModel({
    this.all,
  });

  int all;

  factory CloudsModel.fromRawJson(String str) =>
      CloudsModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CloudsModel.fromJson(Map<String, dynamic> json) => CloudsModel(
        all: json["all"] == null ? null : json["all"],
      );

  Map<String, dynamic> toJson() => {
        "all": all == null ? null : all,
      };
}
