// User status enum
enum UserStatus { active, unauthenticated }

extension UsertStatusExtension on UserStatus {
  int get status {
    switch (this) {
      case UserStatus.active:
        return 1;
      default:
        return 0;
    }
  }
}

// Pod enum
enum Pod { N, D }

// Description enum
enum Description {
  SCATTERED_CLOUDS,
  BROKEN_CLOUDS,
  OVERCAST_CLOUDS,
  FEW_CLOUDS,
  LIGHT_RAIN,
  CLEAR_SKY
}

// Main enum
enum MainEnum { CLOUDS, RAIN, CLEAR }
