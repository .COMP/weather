import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather/bloc/choose_weather_bloc/choose_weather_bloc.dart';
import 'package:weather/ui/common/splash_screen.dart';
import 'package:weather/ui/weather/choose_weather_ui.dart';

final routes = {
  "/splash": (context) => SplashScreen(),
  "/home": (context) => BlocProvider(
        create: (context) => ChooseWeatherBloc(),
        child: ChooseWeatherUi(),
      ),
};
