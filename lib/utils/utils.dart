import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'constant.dart';

class Utils {
  static double iconSize(MediaQueryData mediaQuery) {
    return Orientation.portrait.toString() == mediaQuery.orientation.toString()
        ? mediaQuery.size.width * (6 / 100)
        : mediaQuery.size.width * (6 / 100);
  }

  static double progressSize(MediaQueryData mediaQuery) {
    return Orientation.portrait.toString() == mediaQuery.orientation.toString()
        ? mediaQuery.size.width * (10 / 100)
        : mediaQuery.size.width * (5 / 100);
  }

  static String getImage(String path) => "$ASSETS_IMAGES_PATH$path";

  static String getSvg(String path) => "$ASSETS_SVG_PATH$path";

  static String formatDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "$twoDigitMinutes:$twoDigitSeconds";
  }

  static double verticalSpace(MediaQueryData mediaQuery) {
    return mediaQuery.size.height * (2 / 100);
  }

  static double horizontalSpace(MediaQueryData mediaQuery) {
    return mediaQuery.size.width * (5 / 100);
  }

  static EdgeInsets weatherPagePadding(MediaQueryData mediaQuery) {
    return EdgeInsets.only(
      top: mediaQuery.size.height * 0.07,
      bottom: mediaQuery.size.height * 0.03,
      left: mediaQuery.size.width * 0.08,
    );
  }

  static EdgeInsets paddingCardFooter(MediaQueryData mediaQuery) {
    return Orientation.portrait.toString() == mediaQuery.orientation.toString()
        ? EdgeInsets.symmetric(
            vertical: mediaQuery.size.height * 0.01,
            horizontal: mediaQuery.size.width * 0.05,
          )
        : EdgeInsets.symmetric(
            vertical: mediaQuery.size.height * 0.02,
            horizontal: mediaQuery.size.width * 0.05,
          );
  }

  static backgroundLogoSize(MediaQueryData mediaQuery) {
    return Orientation.portrait.toString() == mediaQuery.orientation.toString()
        ? mediaQuery.size.height * (30 / 100)
        : mediaQuery.size.height * (25 / 100);
  }

  static logoSizeInChoose(MediaQueryData mediaQuery) {
    return Orientation.portrait.toString() == mediaQuery.orientation.toString()
        ? mediaQuery.size.height * (25 / 100)
        : mediaQuery.size.height * (40 / 100);
  }

  static DateTime formatDateTime(DateTime dateTime) {
    final DateFormat formatter = DateFormat(DATE_TIME_FORMAT);
    return DateTime.parse(formatter.format(dateTime));
  }

  static DateTime formatDate(DateTime dateTime) {
    final DateFormat formatter = DateFormat(DATE_FORMAT);
    return DateTime.parse(formatter.format(dateTime));
  }

  static double dayCardWidth(MediaQueryData mediaQuery) {
    return Orientation.portrait.toString() == mediaQuery.orientation.toString()
        ? mediaQuery.size.width * (25 / 100)
        : mediaQuery.size.height * (25 / 100);
  }

  static double dayCardHeight(MediaQueryData mediaQuery) {
    return Orientation.portrait.toString() == mediaQuery.orientation.toString()
        ? mediaQuery.size.height * (12.5 / 100)
        : mediaQuery.size.width * (12.5 / 100);
  }

  static double dayBorderRadius(MediaQueryData mediaQuery) {
    return Orientation.portrait.toString() == mediaQuery.orientation.toString()
        ? mediaQuery.size.width * (5 / 100)
        : mediaQuery.size.height * (5 / 100);
  }

  static EdgeInsets dayCardMargin(MediaQueryData mediaQuery) {
    return EdgeInsets.symmetric(
        horizontal: mediaQuery.size.width * (2 / 100), vertical: 5);
  }

  static double screenHeight(MediaQueryData mediaQuery) {
    return mediaQuery.size.height;
  }

  static double screenWidth(MediaQueryData mediaQuery) {
    return mediaQuery.size.width;
  }

  static double heightLine(MediaQueryData mediaQuery) {
    return Orientation.portrait.toString() == mediaQuery.orientation.toString()
        ? mediaQuery.size.height * 0.007
        : mediaQuery.size.height * 0.015;
  }

  static double widthLine(MediaQueryData mediaQuery) {
    return Orientation.portrait.toString() == mediaQuery.orientation.toString()
        ? mediaQuery.size.width * 0.4
        : mediaQuery.size.width * 0.3;
  }

  static double imageMessage(MediaQueryData mediaQuery) {
    return Orientation.portrait.toString() == mediaQuery.orientation.toString()
        ? mediaQuery.size.width * 0.4
        : mediaQuery.size.width * 0.15;
  }

  static EdgeInsets paddingMessageWidget(MediaQueryData mediaQuery) {
    return Orientation.portrait.toString() == mediaQuery.orientation.toString()
        ? EdgeInsets.symmetric(horizontal: mediaQuery.size.width * 0.1)
        : EdgeInsets.symmetric(horizontal: mediaQuery.size.width * 0.2);
  }
}
