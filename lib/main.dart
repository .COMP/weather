import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get_it/get_it.dart';
import 'package:weather/resources/providers/weather_provider.dart';
import 'package:weather/styles/app_theme.dart';
import 'package:weather/utils/routes.dart';

import 'controller/data_store.dart';
import 'localization/app_localization.dart';

Future<void> initialize() async {
  await DataStore.instance.init();
  GetIt.I.allowReassignment = true;
  GetIt.I.registerSingleton<WeatherProvider>(WeatherProvider());
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initialize();
  try {
    print('language loaded is : ${DataStore.instance.lang}');
    if (!DataStore.instance.hasToken) {
      print('there is no user');
    } else {
      print('user token is : ${DataStore.instance.token}');
    }
  } finally {
    // runApp(DevicePreview(builder: (context)=>AppMaterial()));
    runApp(DevicePreview(enabled: false, builder: (context) => AppMaterial()));
  }
}

class AppMaterial extends StatefulWidget {
  @override
  _AppMaterialState createState() => _AppMaterialState();

  static of(BuildContext context) {
    return context.findAncestorStateOfType<_AppMaterialState>().refresh();
  }
}

class _AppMaterialState extends State<AppMaterial> {
  void refresh() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Weather",
      initialRoute: "/splash",
      routes: routes,
      theme: appTheme,
      debugShowCheckedModeBanner: false,
      supportedLocales: [
        const Locale('ar'),
        const Locale('en'),
      ],
      localizationsDelegates: [
        const AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      locale: Locale(DataStore.instance.lang),
      localeResolutionCallback: (Locale locale, Iterable<Locale> locales) {
        for (Locale supportedLocale in locales) {
          if (supportedLocale.languageCode == locale.languageCode) {
            return supportedLocale;
          }
        }
        return locales.first;
      },
    );
  }
}
