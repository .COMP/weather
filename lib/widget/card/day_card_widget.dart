import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:weather/styles/app_colors.dart';
import 'package:weather/utils/utils.dart';

class DayCardWidget extends StatelessWidget {
  final DateTime value;
  final DateTime selectedValue;
  final Function onTap;

  const DayCardWidget({this.value, this.selectedValue, this.onTap});

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);

    return Container(
      width: Utils.dayCardWidth(mediaQuery),
      height: Utils.dayCardHeight(mediaQuery),
      margin: Utils.dayCardMargin(mediaQuery),
      child: Material(
        shadowColor: AppColors.grayAccent,
        elevation: 2,
        color: this.value == this.selectedValue
            ? AppColors.darkBlue
            : AppColors.white,
        borderRadius: BorderRadius.circular(
          Utils.dayBorderRadius(mediaQuery),
        ),
        child: InkWell(
          borderRadius: BorderRadius.circular(
            Utils.dayBorderRadius(mediaQuery),
          ),
          onTap: this.onTap,
          child: Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                Utils.dayBorderRadius(mediaQuery),
              ),
              color: Colors.transparent,
            ),
            child: Text(
              DateFormat("dd\nMMMM").format(
                value,
              ),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline6.copyWith(
                  color: this.value == this.selectedValue
                      ? AppColors.white
                      : AppColors.lightBlue),
            ),
          ),
        ),
      ),
    );
  }
}
