import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:weather/localization/app_localization.dart';
import 'package:weather/styles/app_colors.dart';
import 'package:weather/utils/utils.dart';

class NetworkErrorWidget extends StatelessWidget {
  final String message;
  final Function onRefresh;

  const NetworkErrorWidget({this.message, @required this.onRefresh});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // image
          Opacity(
            opacity: 0.5,
            child: SvgPicture.asset(
              Utils.getSvg(
                "error.svg",
              ),
              width: Utils.imageMessage(MediaQuery.of(context)),
            ),
          ),
          SizedBox(
            height: Utils.verticalSpace(MediaQuery.of(context)),
          ),

          // error message
          Text(
            message ?? AppLocalization.of(context).trans("error_message"),
            style: Theme.of(context)
                .textTheme
                .bodyText1
                .copyWith(color: AppColors.redAccent),
            textAlign: TextAlign.center,
          ),

          // refresh icon
          IconButton(
            icon: Icon(CupertinoIcons.refresh_bold),
            onPressed: this.onRefresh,
          )
        ],
      ),
    );
  }
}
