import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:weather/localization/app_localization.dart';
import 'package:weather/styles/app_colors.dart';
import 'package:weather/utils/utils.dart';

class EmptyWidget extends StatelessWidget {
  final Function onRefresh;

  const EmptyWidget(this.onRefresh);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: Utils.paddingMessageWidget(MediaQuery.of(context)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // image
            Opacity(
              opacity: 0.5,
              child: SvgPicture.asset(
                Utils.getSvg(
                  "empty.svg",
                ),
                width: Utils.imageMessage(MediaQuery.of(context)),
              ),
            ),
            SizedBox(
              height: Utils.verticalSpace(MediaQuery.of(context)),
            ),

            // empty message
            Text(
              AppLocalization.of(context).trans("no_data"),
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(color: AppColors.lightGray),
              textAlign: TextAlign.center,
            ),

            // refresh icon
            IconButton(
              icon: Icon(CupertinoIcons.refresh_bold),
              onPressed: this.onRefresh,
            )
          ],
        ),
      ),
    );
  }
}
