import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:weather/styles/app_colors.dart';
import 'package:weather/utils/utils.dart';

class ProgressWidget extends StatelessWidget {
  final Color color;

  ProgressWidget({this.color});

  @override
  Widget build(BuildContext context) {
    return SpinKitCircle(
      color: this.color ?? AppColors.white,
      size: Utils.progressSize(MediaQuery.of(context)),
    );
  }
}
