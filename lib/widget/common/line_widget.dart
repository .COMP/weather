import 'package:flutter/material.dart';
import 'package:weather/styles/app_colors.dart';
import 'package:weather/utils/utils.dart';

class LineWidget extends StatelessWidget {
  final double width;

  const LineWidget({Key key, this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Utils.widthLine(MediaQuery.of(context)),
      height: this.width ?? Utils.heightLine(MediaQuery.of(context)),
      decoration: BoxDecoration(
        color: AppColors.grayAccentLight,
        borderRadius: BorderRadius.circular(10),
      ),
    );
  }
}
